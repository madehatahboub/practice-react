import './App.css';
import Footer from './components/Footer';
import Nav from './components/Nav';
import Main from './components/Main';
import Form from './components/Form';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div>
      <Nav />
      <Main />
      <Form/>
      <Footer/>
    </div>
  );
}

export default App;
