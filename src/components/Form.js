import React from "react";

class Form extends React.Component{
    constructor(){
        super()
        this.state = {
            firstName: "",
            lastName: "",
            age: 0,
            gender: "",
            destenation: "",
            dietaryRestrictions: []
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event){
        const {name, value,type,checked} = event.target;
        type === "checkbox" ? this.setState({  checked:value }): this.setState({[name]:value})   
    }

    render(){
        return (
            <div>
                <form className="p-3">
                    <input 
                        className="form-control my-2"
                        placeholder="First Name"
                        type="text" 
                        name="firstName" 
                        value={this.state.firstName} 
                        onChange={this.handleChange}
                    />
                    <input 
                        className="form-control my-2"
                        placeholder="Last Name" 
                        type="text" 
                        name="lastName" 
                        value={this.state.lastName} 
                        onChange={this.handleChange}
                    />
                    <input 
                        className="form-control my-2"
                        placeholder="Age" 
                        type="number" 
                        name="age" 
                        value={this.state.age} 
                        onChange={this.handleChange}
                    />
                    <label>
                        Gender:
                        Female 
                        <input 
                            type="radio" 
                            name="gender" 
                            value="male"
                            checked={this.state.gender == "male"} 
                            onChange={this.handleChange}
                        />
                        Male
                        <input 
                            type="radio" 
                            name="gender" 
                            value="female"
                            checked={this.state.gender == "female"} 
                            onChange={this.handleChange}
                        />
                    </label>

                </form>
                <p>{this.state.firstName} {this.state.lastName}</p>
            </div>
        )
    }
}

export default Form