import React from "react"
import Card from "./Card"

class Main extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            usersData: ""
        }
        this.callAPI = this.callAPI.bind(this)
    }
    callAPI() {
        fetch("http://localhost:4000/users")
            .then(res => {res.text()
             console.log(res);   
            })
            .then(res => this.setState({ usersData: res }))
            .catch(err => console.log(err));
    }
    
    componentDidMount() {
        console.log(this.state.usersData)
        this.callAPI();
        console.log(this.state.usersData)
    }
    
    render(){
        return(
            <div>
                <h1>{this.state.usersData}</h1>
            </div>
        );
    }
}

export default Main