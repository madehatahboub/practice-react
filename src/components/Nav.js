import React from "react"

const Nav = () =>{
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <a className="navbar-brand p-3" href="#">React First App</a>
            <div className="collapse navbar-collapse">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Home</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">SignIn</a>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Nav