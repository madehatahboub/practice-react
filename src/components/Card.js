import React from "react"

function Card(props){
    return(
        <div className="card">
            <h1>{props.name}</h1>
            <p>{props.age}</p>
        </div>
    )
}

export default Card